import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {AppComponent} from './app.component';
import {IplistComponent} from './iplist/iplist.component';
import {AdresaDetailComponent} from './adresa-detail/adresa-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    IplistComponent,
    AdresaDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
