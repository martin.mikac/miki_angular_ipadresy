import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdresaDetailComponent } from './adresa-detail.component';

describe('AdresaDetailComponent', () => {
  let component: AdresaDetailComponent;
  let fixture: ComponentFixture<AdresaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdresaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdresaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
