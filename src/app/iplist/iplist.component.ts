import {Component, OnInit} from '@angular/core';
import {Adresy} from '../Adresy';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-iplist',
  templateUrl: './iplist.component.html',
  styleUrls: ['./iplist.component.css']
})
export class IplistComponent implements OnInit {

  // ipAdresa: Adresy = {ip: '8.8.8.8'};

  selectedAdresa: Adresy;

  selectedAdresaDetail;

  urlApi = '';

ipAdresy: Adresy[] = [
  {ip: '8.8.8.8'},
  {ip: '16.16.16.16'}
];

constructor(private http: HttpClient) {}

ngOnInit() {
}

onSelect(adresa: Adresy): void {
  this.selectedAdresa = adresa;

  /*
  this.http.get('https://api.github.com/users/seeschweiler').subscribe(data => {
    console.log(data);
  });

  this.http.get('https://api.github.com/users/seeschweiler').subscribe(data => {
    this.selectedAdresaDetail = data;
  });
 */

  this.urlApi = 'http://ip-api.com/json/' + this.selectedAdresa.ip;

  this.http.get(this.urlApi).subscribe(data => {
    this.selectedAdresaDetail = data;
  });



}



}
